console.log('Demo');

const user = {
    name : 'Clark Kent',
    description: 'Ceci est une description',
    budget: 100
}

function userBudget(person) {

    if(person.budget === undefined || person.budget === null) {
        person.description = "Tu as oublié ton porte feuille";
    } else if(person.budget > 0 && person.budget < 5){
        person.description = "Il fallait travailler cet été";
    } else if (person.budget === 5) {
        person.description = 'Tu as le droit a une bière';
    } else {
        person.description = 'Tu peux payer ta tournée';
    }
}

userBudget(user);

console.log(`Bravo, ${user.name}, ${user.description}. Voici le rappel de ton budge : ${user.budget}`);