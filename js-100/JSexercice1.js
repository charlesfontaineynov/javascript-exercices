let person;

function getName (firstname, lastname) {
    return firstname + ' ' + lastname;
}

person = getName('Charles', 'Fontaine');
console.log(person);

person = getName('Clark', 'Kent');
console.log(person);

function carre(nbr) {
    return nbr * nbr;
}
console.log(carre(6));